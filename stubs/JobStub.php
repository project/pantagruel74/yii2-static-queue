<?php

namespace Pantagruel74\Yii2StaticQueueStubs;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class JobStub extends BaseObject implements JobInterface
{
    public string $aa;
    public function execute($queue){}
}