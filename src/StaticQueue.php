<?php

namespace Pantagruel74\Yii2StaticQueue;

use yii\queue\JobInterface;
use yii\queue\Queue;

class StaticQueue extends Queue
{
    /* @var string[] $jobs - serialized jobs */
    /* @phpstan-ignore-next-line */
    public static array $jobs = [];

    /**
     * @return JobInterface[]
     */
    public function getAllJobs(): array
    {
        /* @phpstan-ignore-next-line  */
        return array_map(fn(string $s) => ($this->serializer->unserialize($s)), self::$jobs);
    }

    /**
     * @param string $message
     * @param int $ttr time to reserve in seconds
     * @param int $delay
     * @param mixed $priority
     * @return string id of a job message
     */
    protected function pushMessage($message, $ttr, $delay, $priority): string
    {
        self::$jobs[] = $message;
        $keys = array_keys(self::$jobs);
        return end($keys);
    }

    /**
     * @param $id
     * @return int
     */
    public function status($id): int
    {
        return self::STATUS_WAITING;
    }
}