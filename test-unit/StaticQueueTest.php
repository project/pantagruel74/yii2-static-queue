<?php

namespace Pantagruel74\Yii2StaticQueueTestUnit;

use Pantagruel74\Yii2Loader\Yii2Loader;
use Pantagruel74\Yii2StaticQueue\StaticQueue;
use Pantagruel74\Yii2StaticQueueStubs\JobStub;
use PHPUnit\Framework\TestCase;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\queue\Queue;

class StaticQueueTest extends TestCase
{
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    public function testBasics(): void
    {
        $queue = new StaticQueue();
        $this->assertEmpty(StaticQueue::$jobs);

        $testJob = new JobStub(['aa' => 'f781']);

        $id = $queue->push($testJob);
        $queue->push($testJob);
        $this->assertEquals([$testJob, $testJob], $queue->getAllJobs());
        $this->assertEquals(Queue::STATUS_WAITING, $queue->status($id));
    }
}